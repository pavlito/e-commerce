$(document).ready(function() {

   
    try {
        filter_collapse_mobile();
        is_page('.single-product', fancybox);
        sticky_cart();
        cart_plus_minus();
        slider_filter1();
        slider_filter2();
        product_rating();
        user_rating();
        datepicker();
        file_upload();
        accordion_link();

    } catch (e) {
        console.log(e);
    }

     function is_page(bodyClass, functionName) {
            if ($('body').is(bodyClass)) {
                return functionName;
           
        }
    }


     function show_hide_sidebarMenu() {
            var windowWidth = $(window).width();
            var collapsed = $('.filterMenu div[class^="filter-"]').children('.collapse');
            console.log(collapsed);
             if (windowWidth <= 991) {
                $('.filterMenu').removeClass('show');
                collapsed.removeClass('show');
            }

            else {
                $('.filterMenu').addClass('show');
                collapsed.addClass('show');
            }
        }


    function filter_collapse_mobile() {
       
            show_hide_sidebarMenu();
        $(window).on('resize', function(){
            show_hide_sidebarMenu();
        });
        
    }

    function fancybox() {
        $("[data-fancybox]").fancybox({
            infobar: false,
            toolbar: false
        });

        $('.open-album').click(function(e) {
            var el, id = $(this).data('open-id');

            if (id) {
                console.log("ok");
                console.log(id);
                el = $('[rel=' + id + ']:eq(0)');

                e.preventDefault();
                el.click();
            }
        });
    }



    function accordion_link() {
        $("#accordion .card-header a").on("click", function(e) {
            e.preventDefault();
        });
    }

    function file_upload() {
        $('.custom-file-input').on('change', function() {
            var fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        });
    }


    function datepicker() {
        $('.datepicker').datepicker();
    }


    function user_rating() {


        $('.user-rating').barrating({
            theme: 'css-stars',
            showSelectedRating: true,
            readonly: true,
            initialRating: 4
        });


    }

    function product_rating2() {
        $('.product-rating').barrating();
    }

    function product_rating() {

        var currentRating = $('.product-rating').data('currentRating');

        $('.product-current-rating .value').html(currentRating);

        $('.product-rating').barrating({
            theme: 'css-stars',
            initialRating: currentRating,
            showSelectedRating: false,
            onSelect: function(value, text, event) {
                if (typeof(event) !== 'undefined') {
                    // rating was selected by a user
                    $('.product-current-rating').addClass('d-none');
                    $(".product-your-rating .value").html(value);
                    $(".product-your-rating").removeClass('d-none');
                } else {
                    // rating was selected programmatically
                    // by calling `set` method


                }
            }
        });

        $(".br-current-rating").show();
    }

    function sticky_cart() {
        var cart = $(".cart");
        var collapseEl = $(".cart .collapse");
        $(window).on("scroll", function() {
            var offset = $(window).scrollTop();
            if (offset > 200) {
                cart.removeClass('d-none');
            } else {
                cart.addClass('d-none');
            }
        });
        $(document).mouseup(function(e) {
            var container = collapseEl;

            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                collapseEl.collapse('hide');
            }
        });
    }

    function cart_plus_minus() {
        $('.btn-number').click(function(e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function() {
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }

        });
        $(".input-number").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }


    function slider_filter1() {
        var $range = $("#slider-filter-1"),
            $from = $("#input_filter1_min"),
            $to = $("#input_filter1_max"),
            range,
            min = 0,
            max = 100,
            from,
            to;

        var updateValues = function() {
            $from.prop("value", from);
            $to.prop("value", to);
        };

        $range.ionRangeSlider({
            type: "double",
            min: min,
            max: max,
            onStart: function(data) {
                from = data.from;
                to = data.to;

                updateValues();
            },
            onChange: function(data) {
                from = data.from;
                to = data.to;

                updateValues();
            }
        });

        range = $range.data("ionRangeSlider");

        var updateRange = function() {
            range.update({
                from: from,
                to: to
            });
        };

        $from.on("change", function() {
            from = +$(this).prop("value");
            if (from < min) {
                from = min;
            }
            if (from > to) {
                from = to;
            }

            updateValues();
            updateRange();
        });

        $to.on("change", function() {
            to = +$(this).prop("value");
            if (to > max) {
                to = max;
            }
            if (to < from) {
                to = from;
            }

            updateValues();
            updateRange();
        });
    }

    function slider_filter2() {
        var $range = $("#slider-filter-2"),
            $from = $("#input_filter2_min"),
            $to = $("#input_filter2_max"),
            range,
            min = 0,
            max = 100,
            from,
            to;

        var updateValues = function() {
            $from.prop("value", from);
            $to.prop("value", to);
        };

        $range.ionRangeSlider({
            type: "double",
            min: min,
            max: max,
            onStart: function(data) {
                from = data.from;
                to = data.to;

                updateValues();
            },
            onChange: function(data) {
                from = data.from;
                to = data.to;

                updateValues();
            }
        });

        range = $range.data("ionRangeSlider");

        var updateRange = function() {
            range.update({
                from: from,
                to: to
            });
        };

        $from.on("change", function() {
            from = +$(this).prop("value");
            if (from < min) {
                from = min;
            }
            if (from > to) {
                from = to;
            }

            updateValues();
            updateRange();
        });

        $to.on("change", function() {
            to = +$(this).prop("value");
            if (to > max) {
                to = max;
            }
            if (to < from) {
                to = from;
            }

            updateValues();
            updateRange();
        });
    }
});