    'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var del = require('del');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var clean = require('gulp-clean');
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var beep = require('beepbeep');


gulp.task('log', function() {
    gutil.log('test');
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./app",
            index: "index.html",
            injectChanges: true,
        }
    });
});


// gulp.task('clean-libs', function (){
//     return gulp.src('app/libs/*', {read:false})
//     .pipe(clean());
// });

gulp.task('clean-libs', function (){
    return del([
        "app/libs"
    ]);
});

gulp.task('copy', function() {
    return gulp.src([
            'node_modules/bootstrap/dist/**/*',
            'node_modules/ion-rangeslider/**/*',
            'node_modules/jquery-bar-rating/**/*',
            'node_modules/bootstrap-datepicker/**/*',
            'node_modules/@fancyapps/**/*'
        ], { "base": "node_modules/" })
        .pipe(gulp.dest('app/libs/'))
});

gulp.task('sass', function() {
    // var src = gulp.src('./sass/**/*.scss');
    var src = gulp.src('./sass/*.scss');
    var dest = gulp.dest('app/css');
    return src
        .pipe(sourcemaps.init())
        .pipe(sass({ 'outputStyle': 'expanded', 'sync': true }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 5 versions', 'Firefox >= 10', 'iOS >=5', 'ie >= 9'],
            cascade: true
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest)
        .pipe(browserSync.stream());
});

gulp.task('watch:styles', function() {
    gulp.watch('./sass/**/*.scss', gulp.series('sass'), gulp.series(browserSync.reload))
});

gulp.task('watch:html', function() {
    gulp.watch(['app/*.html', 'app/js/*.js']).on('change', gulp.series(browserSync.reload))
});

gulp.task('watch', gulp.series('sass',
    gulp.parallel('watch:styles', 'watch:html')
));

gulp.task('default',gulp.series('clean-libs',  gulp.parallel('copy', 'watch', 'browser-sync')));

gulp.task('img', function() {
    return gulp.src('app/img/**/*')
        .pipe(gulp.dest('dist/img'));
});


gulp.task('clean', function() {
    return del([
        "./dist"
    ]);
});


gulp.task('assets-html', function() {
    return gulp.src('app/*.html')
        .pipe(gulp.dest('dist'));
});

gulp.task('assets-css', function() {
    return gulp.src('app/css/*')
        .pipe(gulp.dest('dist/css'));
});

gulp.task('assets-js', function() {
    return gulp.src('app/js/*')
        .pipe(gulp.dest('dist/js'));
});
gulp.task('assets-libs', function() {
    return gulp.src('app/libs/**/*')
        .pipe(gulp.dest('dist/libs'));
});

gulp.task('assets-data', function() {
    return gulp.src('app/data/*.json')
        .pipe(gulp.dest('dist/data'));
});

gulp.task('assets', gulp.parallel('assets-html', 'assets-css', 'assets-js', 'assets-libs', 'assets-data'));

gulp.task("build", gulp.series(
    "clean",
    gulp.parallel('copy', 'img', 'sass'),
    'assets'
));